package com.getjavajob.training.okhanzhin.streamtests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StreamTest {

    private List<String> stringList;
    private List<Integer> integerList;

    @BeforeEach
    public void init() {
        stringList = Arrays.asList("a1", "a2", "b1", "b1", "c2", "c1", "d1", "d2");
        integerList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
    }

    @Test
    public void testFilter() {
        List<String> excepted = Arrays.asList("a1", "a2");
        assertEquals(excepted, stringList
                .stream()
                .filter(s -> s.startsWith("a")).collect(Collectors.toList()));
    }

    @Test
    public void testFindFirst() {
        String excepted = "a1";
        assertEquals(excepted, stringList
                .stream()
                .findFirst().orElse(null));
    }

    @Test
    public void testFindAny() {
        String excepted = "a1";
        assertEquals(excepted, stringList
                .stream()
                .findFirst().orElse(null));
    }

    @Test
    public void testSkip() {
        List<String> excepted = Arrays.asList("b1", "b1", "c2", "c1", "d1", "d2");
        assertEquals(excepted, stringList
                .stream()
                .skip(2).collect(Collectors.toList()));
    }

    @Test
    public void testDistinct() {
        List<String> excepted = Arrays.asList("a1", "a2", "b1", "c2", "c1", "d1", "d2");
        assertEquals(excepted, stringList
                .stream()
                .distinct().collect(Collectors.toList()));
    }

    @Test
    public void testCount() {
        assertEquals(8, stringList.stream().count());
    }

    @Test
    public void testAnyMatch() {
        assertTrue(stringList.stream().anyMatch("a1"::equals));
    }

    @Test
    public void testAllMatch() {
        List<String> subList = Arrays.asList("a1", "a2", "a3");
        assertTrue(subList.stream().allMatch(s -> s.contains("a")));
    }

    @Test
    public void testNoneMatch() {
        assertTrue(stringList.stream().noneMatch("a4"::equals));
    }

    @Test
    public void testMap() {
        List<String> excepted = Arrays.asList("a1_new", "a2_new", "b1_new", "b1_new",
                                              "c2_new", "c1_new", "d1_new", "d2_new");
        assertEquals(excepted, stringList
                .stream()
                .map(s -> s + "_new")
                .collect(Collectors.toList()));
    }

    @Test
    public void testLimit() {
        List<String> excepted = Arrays.asList("a1", "a2");
        assertEquals(excepted, stringList
                .stream()
                .limit(2)
                .collect(Collectors.toList()));
    }

    @Test
    public void testMin() {
        assertEquals(1, integerList.stream().min(Integer::compareTo).get());
    }

    @Test
    public void testMax() {
        assertEquals(8, integerList.stream().max(Integer::compareTo).get());
    }

    @Test
    public void testToArray() {
        assertArrayEquals(stringList.toArray(), stringList.stream().toArray(String[]::new));
    }

    @Test
    public void testReduce() {
        assertEquals(8, integerList.stream().reduce((i1, i2) -> i1 > i2 ? i1 : i2).get());
    }

    @Test
    public void testSorted() {
        assertEquals(integerList, Stream.of(2,5,4,7,3,1,6,8).sorted().collect(Collectors.toList()));
    }

    @Test
    public void testSum() {
        List<String> stringIntList = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8");
        assertEquals(36, stringIntList.stream().mapToInt(Integer::parseInt).sum());
    }

    @Test
    public void testAverage() {
        List<String> stringIntList = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8");
        assertEquals(4.5, stringIntList.stream().mapToInt(Integer::parseInt).average().getAsDouble());
    }
}
